package com.example.demo.repositories;

import com.example.demo.entities.Categorie;
import com.example.demo.entities.Salle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalleRepository extends JpaRepository<Salle,Long> {
}
