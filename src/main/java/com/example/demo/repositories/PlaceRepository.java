package com.example.demo.repositories;

import com.example.demo.entities.Categorie;
import com.example.demo.entities.Place;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place,Long> {
}
